package com.nttdata.monolito.utils;

import com.nttdata.monolito.dto.EmpleadoDto;
import com.nttdata.monolito.dto.EmpresaDto;
import com.nttdata.monolito.dto.RolDto;
import com.nttdata.monolito.dto.UsuarioDto;
import com.nttdata.monolito.entity.Empleado;
import com.nttdata.monolito.entity.Empresa;
import com.nttdata.monolito.entity.Rol;
import com.nttdata.monolito.entity.Usuario;
import java.util.List;
import java.util.stream.Collectors;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Utils {

  public static Usuario toUsuario(UsuarioDto usuarioDto, Empleado empleado) {
    Usuario usuario = new Usuario();
    usuario.setUsername(usuarioDto.getUsername());
    usuario.setPassword(usuarioDto.getPassword());
    usuario.setEmpleado(empleado);
    return usuario;
  }

  public static Empleado toEmpleado(EmpleadoDto empleadoDto, Empresa empresa, List<Rol> roles) {
    Empleado empleado = new Empleado();
    empleado.setNombre(empleadoDto.getNombre());
    empleado.setSexo(empleadoDto.getSexo());
    empleado.setTelefono(empleado.getTelefono());
    empleado.setEmpresa(empresa);
    empleado.setRoles(roles);
    return empleado;
  }

  public static Rol toRol(RolDto rolDto) {
    Rol rol = new Rol();
    rol.setNombre(rolDto.getNombre());
    return rol;
  }

  public static UsuarioDto toUsuarioDto(Usuario usuario, EmpleadoDto empleadoDto) {
    final UsuarioDto usuarioDto = new UsuarioDto();
    usuarioDto.setUsername(usuario.getUsername());
    usuarioDto.setPassword(usuario.getPassword());
    usuarioDto.setEmpleado(empleadoDto);
    return usuarioDto;
  }

  public static EmpleadoDto toEmpleadoDto(Empleado empleado, EmpresaDto empresaDto, List<RolDto> rolesDto) {
    final EmpleadoDto empleadoDto = new EmpleadoDto();
    empleadoDto.setEmpresa(empresaDto);
    empleadoDto.setNombre(empleado.getNombre());
    empleadoDto.setSexo(empleado.getSexo());
    empleadoDto.setRoles(rolesDto);
    empleadoDto.setTelefono(empleado.getTelefono());
    return empleadoDto;
  }

  public static RolDto toRolDto(Rol rol) {
    final RolDto rolDto = new RolDto();
    rolDto.setNombre(rol.getNombre());
    rolDto.setEmpleados(rol.getEmpleados().stream().map(empleado -> {
      return Utils.toEmpleadoDto(empleado, Utils.toEmpresaDto(empleado.getEmpresa()), null);
    }).collect(Collectors.toList()));
    return rolDto;
  }

  public static Empresa toEmpresa(EmpresaDto empresaDto) {
    Empresa empresa = new Empresa();
    empresa.setRazonSocial(empresaDto.getRazonSocial());
    empresa.setRuc(empresaDto.getRuc());
    empresa.setRepresentante(empresaDto.getRepresentante());
    empresa.setFechaCreacion(empresaDto.getFechaCreacion());
    return empresa;
  }

  public static EmpresaDto toEmpresaDto(Empresa empresa) {
    final EmpresaDto empresaDto = new EmpresaDto();
    empresaDto.setRazonSocial(empresa.getRazonSocial());
    empresaDto.setRuc(empresa.getRuc());
    empresaDto.setRepresentante(empresa.getRepresentante());
    empresaDto.setFechaCreacion(empresa.getFechaCreacion());
    return empresaDto;
  }
}
