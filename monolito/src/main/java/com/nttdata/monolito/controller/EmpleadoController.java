package com.nttdata.monolito.controller;

import com.nttdata.monolito.dto.EmpleadoDto;
import com.nttdata.monolito.entity.Empleado;
import com.nttdata.monolito.service.IEmpleadoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/empleado")
public class EmpleadoController {
    @Autowired
    IEmpleadoService IEmpleadoService;

    @GetMapping("/listar")
    public List<EmpleadoDto> listarEmpleados() {
        return IEmpleadoService.listarEmpleados();
    }

    @GetMapping("/filtrar/nombre")
    public List<EmpleadoDto> filtrarEmpleadosPorNombre(@RequestParam("texto") String texto) {
        return IEmpleadoService.filtrarEmpleadosPorNombre(texto);
    }

    @PostMapping("/registrar")
    public EmpleadoDto registrarEmpleado(@RequestBody EmpleadoDto body) {
        return IEmpleadoService.registrarEmpleado(body);
    }

    @PutMapping("/actualizar/{id}")
    public EmpleadoDto actualizar(@RequestBody EmpleadoDto body, @PathVariable("id") Long idEmpleado) {
        return IEmpleadoService.actualizar(body, idEmpleado);
    }

    @DeleteMapping("/eliminar/{id}")
    public String eliminar(@PathVariable("id") Long idEmpleado) {
        return IEmpleadoService.eliminar(idEmpleado);
    }
}
