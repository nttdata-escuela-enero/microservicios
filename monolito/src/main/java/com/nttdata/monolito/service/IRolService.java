package com.nttdata.monolito.service;

import com.nttdata.monolito.dto.RolDto;
import java.util.List;

public interface IRolService {

  List<RolDto> listarRoles();

  RolDto registrarRol(RolDto body);

  RolDto actualizar(RolDto body, Long idRol);

  String eliminar(Long idRol);
}
