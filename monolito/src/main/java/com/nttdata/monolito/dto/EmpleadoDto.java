package com.nttdata.monolito.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.io.Serializable;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class EmpleadoDto implements Serializable {

  private String nombre;

  private String sexo;

  private String telefono;

  private Long empresaId;

  private EmpresaDto empresa;

  private List<Long> rolesId;

  private List<RolDto> roles;

  private String message;

  public EmpleadoDto(String message) {
    this.message = message;
  }
}
